<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function yg_biz_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['yg_biz_settings'] = [
    '#type' => 'details',
    '#title' => t('YG Biz Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'bootstrap',
    '#weight' => 10,
  ];
  // Social links.
  $form['yg_biz_settings']['social_links'] = [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_biz_settings']['social_links']['facebook_url'] = [
    '#type' => 'url',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['yg_biz_settings']['social_links']['twitter_url'] = [
    '#type' => 'url',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['yg_biz_settings']['social_links']['youtube'] = [
    '#type' => 'url',
    '#title' => t('Youtube'),
    '#description' => t('Please enter your youtube url'),
    '#default_value' => theme_get_setting('youtube'),
  ];
  $form['yg_biz_settings']['social_links']['instagram_url'] = [
    '#type' => 'url',
    '#title' => t('Instagram'),
    '#description' => t('Please enter your Instagram url'),
    '#default_value' => theme_get_setting('instagram_url'),
  ];
  $form['yg_biz_settings']['social_links']['pinterest_url'] = [
    '#type' => 'url',
    '#title' => t('Pinterest'),
    '#description' => t('Please enter your Pinterest url'),
    '#default_value' => theme_get_setting('pinterest_url'),
  ];
  // Contact details.
  $form['yg_biz_settings']['contact_info'] = [
    '#type' => 'details',
    '#title' => t('Contact Info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $address = theme_get_setting('address');
  $form['yg_biz_settings']['contact_info']['address'] = [
    '#type' => 'text_format',
    '#title' => t('Left-Footer Description'),
    '#description' => t('Please enter left side footer description...'),
    '#default_value' => $address['value'],
    '#foramt'        => $address['format'],
  ];
  $form['yg_biz_settings']['contact_info']['phone_no'] = [
    '#type' => 'textfield',
    '#title' => t('Phone Number'),
    '#description' => t('Please enter your phone number'),
    '#default_value' => theme_get_setting('phone_no'),
  ];
  $form['yg_biz_settings']['contact_info']['email'] = [
    '#type' => 'email',
    '#title' => t('Email Id'),
    '#description' => t('Please enter your email-id'),
    '#default_value' => theme_get_setting('email'),
  ];
  $form['yg_biz_settings']['contact_info']['time'] = [
    '#type' => 'textfield',
    '#title' => t('Office Time'),
    '#description' => t('Please enter your office-time'),
    '#default_value' => theme_get_setting('time'),
  ];
  // call-to-action.
  $form['yg_biz_settings']['call_to_action'] = [
    '#type' => 'details',
    '#title' => t('Call-to-action'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_biz_settings']['call_to_action']['cta_link'] = [
    '#type' => 'textfield',
    '#title' => t('Call-to-action Url'),
    '#description' => t('Please enter call-to-action url'),
    '#default_value' => theme_get_setting('cta_link'),
  ];
  $cta_desc = theme_get_setting('cta_desc');
  $form['yg_biz_settings']['call_to_action']['cta_desc'] = [
    '#type' => 'text_format',
    '#title' => t('Call-to-action Description'),
    '#description' => t('Please enter the footer description...'),
    '#default_value' => $cta_desc['value'],
    '#foramt'        => $cta_desc['format'],
  ];
  // Banner.
  $form['yg_biz_settingss'] = [
    '#type' => 'details',
    '#title' => t('Banner Image'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_biz_settingss']['bg-image'] = [
    '#type' => 'managed_file',
    '#title'    => t('Background Image'),
    '#default_value' => theme_get_setting('bg-image'),
    '#upload_location' => 'public://',
    '#description' => t('Choose your background image for all pages'),
  ];
  $form['#submit'][] = 'yg_biz_form_submit';
}

/**
 * Implements hook_form_submit().
 */
function yg_biz_form_submit(&$form, $form_state) {

  $fid = $form_state->getValue('bg-image');
  if (array_key_exists(0, $fid)) {
    $file = File::load($fid[0]);
    if (!empty($file)) {
      $file->setPermanent();
      $file->save();
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'yg_biz', 'themes', 1);
    }
  }
}
